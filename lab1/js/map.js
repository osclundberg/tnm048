function map(){


    var cc = [];

    var zoom = d3.behavior.zoom()
        .scaleExtent([0.1, 8])
        .on("zoom", move);

    var mapDiv = $("#map");

    var margin = {top: 20, right: 20, bottom: 20, left: 20},
        width = mapDiv.width() - margin.right - margin.left,
        height = mapDiv.height() - margin.top - margin.bottom;

    //initialize color scale
    //...
    
    //initialize tooltip
    //...

    var projection = d3.geo.mercator()
        .center([50, 60 ])
        .scale(250);

    var svg = d3.select("#map").append("svg")
        .attr("width", width)
        .attr("height", height)
        .call(zoom);

    var path = d3.geo.path()
        .projection(projection);

    g = svg.append("g");

    // load data and draw the map
    d3.json("data/world-topo.json", function(error, world) {
        var countries = topojson.feature(world, world.objects.countries).features;

        //load summary data
        //...


        draw(countries);


        d3.selectAll(".foreground path").style("stroke", function(d,i){
           for(var j=0; j<countries.length;j++){
              if(pc1.data[i]["Country"] === cc[j].id){
                  return cc[j].color;
                }

            }
        });
        
        d3.selectAll("#sp circle").style("fill", function(d,i){
            for(var j=0; j<countries.length;j++){
                if(sp1.data[i]["Country"] === cc[j].id){
                    return cc[j].color;
                }

            }
            // return "rgb(44,44,44)";
        });
        
    });
    function randomColor(){

        var r= Math.round(Math.random()*255);
        var g= Math.round(Math.random()*255);
        var b= Math.round(Math.random()*255);

        return "rgb("+r+","+g+","+b+")";
    }

    function draw(countries,data)
    {
        var country = g.selectAll(".country").data(countries);
        //initialize a color country object	

        var check = false;

        for (var i = 0; i < countries.length; i++) {
            check =false;
            for(var j =0;j<sp1.data.length; j++){
                    if(sp1.data[j]["Country"] == countries[i].properties.name){
                        cc.push({id: countries[i].properties.name, color: randomColor()})
                        check = true;
                        break;
                    }
            };
            if(!check){
                cc.push({id: countries[i].properties.name, color: "rgb(144,144,144)"})
            }    
        }
        //...

        country.enter().insert("path")
            .attr("class", "country")
            .attr("d", path)
            .attr("id", function(d) { return d.id; })
            .attr("title", function(d) { return d.properties.name; })
            //country color
            .style("fill", function(d,i){
                        return cc[i].color;
            })
            //tooltip
            .on("mousemove", function(d) {
                //...
            })
            .on("mouseout",  function(d) {
                //...
            })
            //selection
            .on("click",  function(d) {
                sp1.selectDot(d.properties.name);
                pc1.selectLine(d.properties.name);
                map.selectLine(d.properties.name);
            });

    }
    
    //zoom and panning method
    function move() {

        var t = d3.event.translate;
        var s = d3.event.scale;
        

        zoom.translate(t);
        g.style("stroke-width", 1 / s).attr("transform", "translate(" + t + ")scale(" + s + ")");

    }

    this.getCC = function(){
        return cc;
    }
    
    //method for selecting features of other components
    function selFeature(value){

    }


    this.selectLine =  function (value){

        var country = d3.selectAll("#map path").filter(function(d){
            return d.properties.name==value;
        })

        country = country[0][0];
            
            if(d3.selectAll("#map .opac")[0].length == 239 && d3.select(country).classed("country")){
                d3.selectAll("#map .opac").attr("class", "country");
                console.log(1)
            }
            else if(d3.select(country).classed("opac")){
                d3.select(country).attr("class","country");
                console.log(2)
            }
            else if(d3.select(country).classed("country") &! d3.selectAll("#map .opac").empty()){
                d3.select(country).attr("class","opac");
                console.log(3)
            }
            else{
              d3.selectAll("#map path").attr("class", "opac");
              d3.select(country).attr("class","country");
              console.log(4)

            }

    }

        this.selectProxyLine = function(country, inBounds){
        var marked = d3.selectAll("#map path").filter(function(d){
             return d.properties.name==country;
        })

        marked = marked[0][0];

        (inBounds) ? d3.select(marked).attr("class", "country") : d3.select(marked).attr("class", "opac");
    };
       

}

