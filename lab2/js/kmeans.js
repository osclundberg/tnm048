    /**
    * k means algorithm
    * @param data
    * @param k
    * @return {Object}
    */
    var centroids = [];
    var centroidsDistance = [];
   
    function kmeans(data, k) {

        var dim = Object.keys(data[0]).length

        var indexArr = new Array(data.length);

        extentA = d3.extent(data, function(d) {
            var max = +d.A
             return max.toPrecision(5); 
        })

        extentB = d3.extent(data, function(d) {
            var max = +d.B
             return max.toPrecision(5); 
        })

        extentC = d3.extent(data, function(d) {
            var max = +d.C
             return max.toPrecision(5); 
        })
    	
       for (var i = 0; i < k; i++) {
           centroids.push(getRandomCentroid(dim));
           centroidsDistance.push(10000);
       };

        // centroids.push([1,1,1]);
        // centroids.push([0.9,0.9,0.9]);
        // centroids.push([0,0,0]);

        var gatherArray = [];

        var threshold = 1;
        var minError = 0;
		var newQualityChecker = 10000;
		var counter = 0;
     // begin while
        while(minError != threshold){


            gatherArray = [];

            for (var i = 0; i < k; i++)
                gatherArray.push([]);
			
			//Which dot that represents which centroid
            for (var i = 0; i < data.length; i++) {
                indexArr[i] = calculateCentroidDistances(data[i]);
                gatherArray[calculateCentroidDistances(data[i])].push(data[i]);
            };
			
			//Make the new centroids
            centroids = [];
            for (var i = 0; i < k; i++) {
                centroids[i] = calculateNewCentroid(gatherArray[i]);
			  };
			
			//Calculate the quality value and look the diffrence beetween prev and current
			var prevQuality = newQualityChecker
            newQualityChecker = quialityChecker(gatherArray);			
			minError = newQualityChecker/prevQuality;
			
            // console.log(newQualityChecker)
            counter++;
        }

		// console.log("Iterations: " + counter);
    return indexArr;
    // while end    

    };

    Array.prototype.mean = function map(){
        if(this.length == 0){
            return 0;
        }

        return (this.reduce(function(a,b){
            return +a + +b;
        })) / this.length;
    };



    function calculateNewCentroid(centroidData){
        var a = [], b =[], c=[], d=[], f=[];

        for (var i = 0; i < centroidData.length; i++) {
            a.push(centroidData[i].A);
            b.push(centroidData[i].B);
            c.push(centroidData[i].C);
            d.push(centroidData[i].D);
            f.push(centroidData[i].F);
        };
       return [a.mean(), b.mean(), c.mean(), d.mean(), f.mean()];

    }



    function calculateCentroidDistances(p){
        var distances = [];
        for (var i = 0; i < centroids.length; i++) {

             distances.push(Math.sqrt(Math.pow(p.A - centroids[i][0], 2) +
             Math.pow(p.B - centroids[i][1], 2) +
             Math.pow(p.C - centroids[i][2], 2))
             );
         }; 
         var min = d3.min(distances);
         var index = distances.indexOf(min);

         return index;
    }

    function quialityChecker(gatherArray){

        var sum = 0;
        for(var i = 0; i < centroids.length; i++){
            for (var j = 0; j < gatherArray[i].length; j++) {
                 sum += (Math.pow(gatherArray[i][j].A - centroids[i][0], 2) +
                 Math.pow(gatherArray[i][j].B - centroids[i][1], 2) +
                 Math.pow(gatherArray[i][j].C - centroids[i][2], 2))
                  // console.log("centroids[i]: " + centroids[i] + " gatherArray[i][j]" + gatherArray[i][j].A
                  //   + " " + gatherArray[i][j].B + " " + gatherArray[i][j].C)
                 // console.log(sum)
            }
            
            
        }
        return sum;
    }

    function getRandomCentroid(dimensions){
        var a = []
        for (var i = 0; i < dimensions; i++) {
            a.push(Math.random());
        };
        return a;
    }

    function calculateMeans(gatherArray, dim){
            // console.log('this')
            return 0;

    }