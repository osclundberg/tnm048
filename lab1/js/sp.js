function sp(){

    var self = this; // for internal d3 functions

    var spDiv = $("#sp");
    // $('#sp').width("1000px");

    var margin = {top: 20, right: 20, bottom: 30, left: 40},
        width = spDiv.width() - margin.right - margin.left,
        height = spDiv.height() - margin.top - margin.bottom;

    //initialize color scale
    //...
    
    //initialize tooltip
    //...
    var tooltip = d3.select("#sp").append("div")   
    .attr("class", "tooltip").style("display","none")  


    var x = d3.scale.linear()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var svg = d3.select("#sp").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    


    //Load data
    d3.csv("data/OECD-better-life-index-hi.csv", function(error, data) {
        self.data = data;

        y.domain([0,d3.max(data, function(d){
            return d["Household income"];
            // Household income
        })]);
        x.domain([0, d3.max(data, function(d){
            return d["Life satisfaction"];
        })])
        //define the domain of the scatter plot axes
        //...
        
        draw();
    });



    function draw()
    {

        // Add x axis and title.
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .attr("text-anchor", "end")
            .text("Life satisfaction");
            
        // Add y axis and title.
        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .attr("text-anchor", "end")
            .text("Household earnings");
        

        // Add the scatter dots.
        svg.selectAll(".dot")
            .data(self.data)
            .enter().append("circle")
            .attr("class", "dot")
            //Define the x and y coordinate data values for the dots
            .attr("r", function(d){
                return Math.round(d["Personal earnings"]/2);
            })
            .attr("cx", function(d){
                return x(d["Life satisfaction"]);
            })
            .attr("cy", function(d){
                return y(d["Household income"]);
            })
            //...
            //tooltip
            .on("mousemove", function(d) {
                var el = this;

                $('#sp .tooltip').show()
                var el = this;
                d3.select("#sp .tooltip")
                .style("left", (d3.event.pageX-40) + "px")     
                .style("top", (d3.event.pageY -100) + "px")
                    //.attr('class', 'd3-tip')
                    .html(function(){
                        return d["Country"];
                    })
            })
            .on("mouseout", function(d) {
                $('#sp .tooltip').hide()  
            })
            .on("click",  function(d) {

                pc1.selectLine(d["Country"]);
                map.selectLine(d["Country"]);
                self.selectDot(d['Country']);
            });
    }

    //method for selecting the dot from other components

    this.selectDot = function(value){

        var clicked = d3.selectAll("circle").filter(function(d){
             return d['Country']==value;
        })
            clicked = clicked[0][0];

            if(d3.selectAll("#sp .opac")[0].length == 35 && d3.select(clicked).classed("dot")){
                d3.selectAll("#sp .opac").attr("class", "dot");
            }
            else if(d3.select(clicked).classed("opac")){
                d3.select(clicked).attr("class","dot");
            }
            else if(d3.select(clicked).classed("dot") &! d3.selectAll("#sp .opac").empty() ){
                d3.select(clicked).attr("class","opac");
            }
            else{
              d3.selectAll(".dot").attr("class", "opac");
              d3.select(clicked).attr("class","dot");
            }
    };

    this.selectProxyDot = function(country, inBounds){

        var marked = d3.selectAll("circle").filter(function(d){
             return d['Country']==country;
        })

        marked = marked[0][0];

        (inBounds) ? d3.select(marked).attr("class", "dot") : d3.select(marked).attr("class", "opac");
    };
                

    
    //method for selecting features of other components
    function selFeature(value){
        //...
    }

}




